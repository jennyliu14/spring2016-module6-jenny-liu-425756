var usernames = [];
var rooms = ["default"];
var passwords = [""];
var roomuserlookup = [];
var roompasswordlookup = [];
var userroomlookup = [];
var usersocketIDslookup = [];

roompasswordlookup["default"] = "";

// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.

	socket.join("default");
 
	socket.on('message_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 
		console.log("message: "+data["message"] + "to user: " + data['user'] + " in socket room " + socket.room); // log it to the Node.JS output
		io.sockets.in(socket.room).emit("message_to_client",{message:data["message"], user:data["user"]}); // broadcast the message to other users
	});

	socket.on('login_to_server', function(data) {
		// This callback runs when the server receives a new message from the client.
 		usernames[data["user"]] = socket.id;
 		socket.room = "default";
 		socket.nickname = data["user"];
 		socket.join("default");
 		console.log("THE SOCKETS NICKNAME IS " + socket.nickname);
		console.log("username: "+data["user"] + "socket.id " + socket.id); // log it to the Node.JS output
		io.sockets.emit("login_to_client",{user:data["user"] }); // broadcast the message to other users. {user:data["user"]} is recognized as data on the client side
	});

	socket.on('createroom_to_server', function(data) {
		rooms[rooms.length] = data['roomname'];
		passwords[passwords.length] = data['roompassword'];
		roomuserlookup[data["roomname"]] = data["user"];
		roompasswordlookup[data["roomname"]] = data["roompassword"];
		console.log("roomname: "+data["roomname"]);
		io.sockets.to(socket.id).emit("createroom_to_client",{roomname:data["roomname"]});
	});

	socket.on('listrooms_to_server', function(data) {
		io.sockets.emit("clearlistofrooms_to_client");
		for (var i = 0; i < rooms.length; i++) {
			io.sockets.emit("listrooms_to_client", {roomname:rooms[i]});
		}
	});

	socket.on('joinroom_to_server', function(data) {
		if (roompasswordlookup[data['roomname']] == data['roompassword']) {
			socket.leave(socket.room);
			socket.join(data['roomname']);
			socket.room = data['roomname'];
			console.log("client " + socket.id + " with nickname " + socket.nickname + " joined room " + socket.room);
			io.sockets.to(socket.id).emit("updateroom_to_client", {roomname:data['roomname']});
		}
	});
	function usersbyroom(roomId) {
	//This function creates an array of clients in a room
	console.log("looking through room name" + roomId);
		var res = [], room = io.sockets.adapter.rooms[roomId];
	if (room) {
		for (var id in room) {
			io.nsps["/"].adapter.rooms[roomName]
			res.push(io.sockets.adapter.nsp.connected[id]);
			console.log("id " + id + " is " + io.sockets.adapter.nsp.connected[id]);
		}
	}
	return res;
	}

	socket.on('updatepeople_to_server', function(data){
		console.log("listing people in room " + data['roomname']);
		// io.sockets.in(data['room']).emit("clearlistofpeople_to_client", "");
		// var roster = usersbyroom(data['roomname']);
		var roster = io.nsps["/"].adapter.rooms[data['roomname']];
		var rosterSockets = Object.keys(roster.sockets);
		console.log("roster " + rosterSockets + "and length " + rosterSockets.length);
		// roster.forEach(function(client) {

		for (var i = 0; i < rosterSockets.length; i++) {
			console.log("id: " + rosterSockets[i]);
			var nickname = io.sockets.connected[rosterSockets[i]].nickname;
			console.log("nickname is " + nickname);
			// console.log(Object.keys(temp));
			io.sockets.in(data['roomname']).emit("updatepeople_to_client", {user:nickname});
		}
			
		// 	console.log("client nick name" + client.nickname);
		// 	io.sockets.in(data['roomname']).emit("updatepeople_to_client", {name:client.nickname}); //sends to all in room
		// });
		// for (var i = 0; i < roster.length; i++) {
		// 	// console.log("client nick name" + Object.);
		// 	// io.sockets.in(data['roomname']).emit("updatepeople_to_client", {name:roster[i].nickname});
		// }
	});
	

});


